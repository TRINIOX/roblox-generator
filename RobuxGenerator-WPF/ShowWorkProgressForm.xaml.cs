﻿using System;
using System.Threading;
using System.Windows;

namespace RobuxGenerator_WPF
{
    /// <summary>
    /// Interaktionslogik für ShowWorkProgressForm.xaml
    /// </summary>
    public partial class ShowWorkProgressForm : Window
    {
        public ShowWorkProgressForm(string userName)
        {
            InitializeComponent();

            this.Title = $"Generating Robux on {userName}..";

            double max = workProgressBar.Maximum;

            new Thread(() =>
            {
                for (int i = 0; i < max; i+= new Random().Next(10))
                {
                    Dispatcher.Invoke(new Action(() => workProgressBar.Value = i));
                    Thread.Sleep(1000);
                }
                Dispatcher.Invoke(new Action(() => 
                {
                    DialogResult = true;
                }));
            }).Start();
        }
    }
}
